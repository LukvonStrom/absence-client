# absence-client

## TODO
- Migrate to [Preact- CLI Boilerplate](https://github.com/developit/preact-cli)
    - Include `fetch` Polyfill when needed.
    - Include `Promise` Polyfill if nescessary
- Reimplement the ErrorBoundary when possible. Track [Preact #886](https://github.com/developit/preact/pull/886)
- Migrate to Context whenever possible
- Implement [React Router](https://reacttraining.com/react-router/web/example/auth-workflow)
- Revisit Internet Explorer 11 Flexbox Bugs
- Implement Error Response for logging in from API
- Write Tests.
- ~~Turn off React Devtools for Production Builds~~
