import {isSupported} from "u2f-api/dist/index";

export default check = async (triggerError) => {
    const support = await isSupported();
    if (!support) return triggerError("U2F aktiviert, aber Gerät inkompatibel!");

    const SignRequestRaw = await fetch('http://localhost:8080/u2f/auth', {
        method: "GET",
        headers: {
            Authorization: json.token,
        }
    });

    const SignResponse = await sign({registerRequests: SignRequestRaw.json()['Operation']});

    const rawSuccess = await fetch('http://localhost:8080/u2f/auth', {
        method: "POST",
        headers: {
            Authorization: json.token,
        },
        body: JSON.stringify({SignResponse})
    });

    const success = rawSuccess.json();

    if (!success.Operation.success) {
        return triggerError(success.Operation.message);
    }
}