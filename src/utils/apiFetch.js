const apiURL = 'http://localhost:8080';
export default function apiFetch(endpoint, method) {
    return new Promise((resolve, reject) => {
        if(!localStorage.getItem("token")) reject(new Error("Unauthorized"));
        fetch(`${apiURL}/${endpoint}`, {
            method,
            headers: {
                Authorization: localStorage.getItem("token"),
            }
        }).then((response) => {
                if(response.ok && !(response.status === 401) && !(response.status === 500)) return response.json();
                throw new Error("Unauthorized")
            })
            .then(resolve)
            .catch(reject)
    })

}