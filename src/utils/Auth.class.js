import {route} from "preact-router";

export default class Authentication {

    /**
     * Authenticate the User by saving in Local Storage
     *
     * @author Lukas Fruntke
     */
    static authenticate({token, user}) {
        // perform vodoo
        localStorage.setItem('token', token);
        localStorage.setItem('name', user.name);
        localStorage.setItem('grade', user.grade);
        route('/absence');
    }

    static getUserInfo() {
        return {
            name: localStorage.getItem('name'),
            grade: localStorage.getItem('grade'),
        }
    }

    /**
     * Check if User is authenticated
     *
     * @author Lukas Fruntke
     * @returns {boolean}
     */
    static isAuthenticated() {

        return localStorage.getItem('token') !== null;

    }

    /**
     * Deauthenticate User by removing from Local Storage
     *
     * @author Lukas Fruntke
     */
    static deauthenticate() {
        ['token', 'name', 'grade'].forEach((i) => localStorage.removeItem(i));
        route('/?logout=true', true);

    }

    /**
     * Read the saved Token from Local Storage
     *
     * @author Lukas Fruntke
     * @returns {string | null}
     */
    static getToken() {

        return localStorage.getItem('token');

    }

    /**
     * Reauthenticate User with new Token
     *
     * @author Lukas Fruntke
     * @param newToken
     */
    static reauthenticate(newToken) {

        this.deauthenticate();
        this.authenticate(newToken);

    }

}

