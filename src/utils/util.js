import isEmptyObject from './utility/isEmptyObject';
import getAllUrlParams from './utility/getAllURLParams';

export default {
    isEmptyObject,
    getAllURLParams: getAllUrlParams
};

