
import { h } from 'preact';

export default function LoginContextComponent() {

    return (
        <main>
            <h1>Fehlzeitenverfahren Baden-Würrtemberg</h1>
            <p>„Jeder Schüler ist verpflichtet, den Unterricht und die übrigen verbindlichen
                Veranstaltungen der Schule regelmäßig und ordnungsgemäß zu besuchen.“
            </p>
            <p> (<a href="http://www.landesrecht-bw.de/jportal/?quelle=jlink&query=SchulBesV+BW&psml=bsbawueprod.psml&max=true&aiz=true#jlr-SchulBesVBWV5P1"><u>§1 Abs. 1 Schulbesuchsverordnung</u></a>)</p>
            <br /><h2>Entschuldigungsregelung</h2>

            <p>Im Falle einer fernm&uuml;ndlichen oder <b>elektronischen</b>&nbsp;
                Verständigung der Schule <b>bis zum zweiten Tag</b> ist
                die schriftliche Mitteilung innerhalb von drei weiteren
                Tagen nachzureichen.
            </p>
            <p>Trägst du dich also am Tag des Fehlens oder am Tag danach hier im System ein,
                so musst du in sp&auml;testens <b>drei</b> Tagen eine Entschuldigung
                bei deinem Tutor abgegeben haben.
            </p>
            <p>Andernfalls bist du <b>unentschuldigt</b>.</p>
        </main>
    );

}

