import Authentication from '../../../utils/Auth.class';

export default async function loginRequest(email, pass, triggerError) {
    try{
    const request = await fetch('http://localhost:8080/login', {
        body: JSON.stringify({email, password: pass}),
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'content-type': 'application/json'
        },
        method: 'POST',
        mode: 'cors',
        redirect: 'follow',
        referrer: 'no-referrer'
    });
    const jsonResp = await request.json();
    const json = jsonResp.Operation;
    console.log(json, jsonResp, request);
    if (json.status === "Success" && json.token !== false) {

        if (json.u2f_enabled) {
            await check(triggerError);
            }

        return Authentication.authenticate({token: json.token, user: json.user});

    }

    triggerError(json.message);

}catch(err){
            console.error(err);
            const errorTranslation = { 'Failed to fetch': 'Netzwerkfehler, sind sie mit dem Internet verbunden?' };
            triggerError((err.message in errorTranslation ?
                errorTranslation[err.message] : err.message));
        }

}

