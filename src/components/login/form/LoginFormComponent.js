import { h, Component } from 'preact';
import loginRequest from './loginRequest';

export default class Form extends Component {

    /**
     *
     * @param props loginErrorMSG  => If first char is + it is not displayed with a "Fehler: " Prefix but can be used as a announcement box.
     */
  constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            loginError: null,
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.triggerError = this.triggerError.bind(this);
        if(props.loginErrorMSG) this.triggerError(props.loginErrorMSG)
    }


    async handleSubmit(e) {

        e.preventDefault();

        try {

            await loginRequest(this.state.email, this.state.password, (e) => {this.setState({loginError: e})});

        } catch (e) {

            console.error(e);

        }
    }

    triggerError(erro) {
      this.setState({loginError: erro})
    };

    handleInputChange(event) {

        const value = event.target.value;
        const name = event.target.id;

        this.setState({ [name]: value });

    }


    render(props, {loginError, email, password}) {

        return (
            <div id="login">
                <form onSubmit={this.handleSubmit}>
                    {loginError && <div id="login_error" style={{'color': 'red'}}><p>{loginError.charAt(0) === "+" ? `${loginError.replace('+', '')}` : `Fehler: ${loginError}`}</p></div>}
                    <input id="email" type="text" placeholder="Email" value={email} onChange={this.handleInputChange} />
                    <input id="password" type="password" placeholder="Passwort" value={password} onChange={this.handleInputChange} />
                    <button type="submit">Anmelden</button>
                </form>
            </div>
        );

    }

}
