import { h } from 'preact';

export default function AbsenceComponent() {

    return (
        <main>
            <div class="flex headline">
                <h1>Fehlzeiten&uuml;bersicht</h1>
                <button>Neue Fehlzeit</button>
            </div>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
            sed diam nonumy eirmod tempor invidunt ut labore et
            dolore magna aliquyam erat, sed diam voluptua.
            At vero eos et accusam et justo duo dolores et ea rebum.
            Stet clita kasd gubergren, no sea takimata sanctus est.
            </p>{
                <table>
                    <thead>
                        <tr>
                            <th class="date">Datum</th>
                            <th class="subject">Fach</th>
                            <th class="registered">eingetragen am</th>
                            <th class="approved">bestätigt</th>
                            <th class="reason">Grund</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="date">1. April 2018</td>
                            <td class="subject">Deutsch</td>
                            <td class="registered">1. April 2018</td>
                            <td class="approved">ja</td>
                            <td class="reason">-</td>
                        </tr>
                        <tr>
                            <td class="date">1. April 2018</td>
                            <td class="subject">Deutsch</td>
                            <td class="registered">1. April 2018</td>
                            <td class="approved">ja</td>
                            <td class="reason">-</td>
                        </tr>
                        <tr>
                            <td class="date">1. April 2018</td>
                            <td class="subject">Deutsch</td>
                            <td class="registered">1. April 2018</td>
                            <td class="approved">ja</td>
                            <td class="reason">-</td>
                        </tr>
                        <tr>
                            <td class="date">1. April 2018</td>
                            <td class="subject">Deutsch</td>
                            <td class="registered">1. April 2018</td>
                            <td class="approved">ja</td>
                            <td class="reason">-</td>
                        </tr>
                        <tr>
                            <td class="date">1. April 2018</td>
                            <td class="subject">Deutsch</td>
                            <td class="registered">1. April 2018</td>
                            <td class="approved">ja</td>
                            <td class="reason">-</td>
                        </tr>
                        <tr>
                            <td class="date">1. April 2018</td>
                            <td class="subject">Deutsch</td>
                            <td class="registered">1. April 2018</td>
                            <td class="approved">ja</td>
                            <td class="reason">-</td>
                        </tr>
                        <tr>
                            <td class="date">1. April 2018</td>
                            <td class="subject">Deutsch</td>
                            <td class="registered">1. April 2018</td>
                            <td class="approved">ja</td>
                            <td class="reason">-</td>
                        </tr>
                    </tbody>
                </table> }
        </main>);

}

