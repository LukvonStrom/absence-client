import { h } from 'preact';
import {Link} from "preact-router/match";

export default function NavComponent() {

    return (<nav>
        <ul>
            <li>
                <Link activeClassName="highlighted" href="/absence">Fehlzeiten</Link>
            </li>
            <li>
                <Link activeClassName="highlighted" href="/timetable">Stundenplan</Link>
            </li>
            <li>
                <Link activeClassName="highlighted" href="/settings">Einstellungen</Link>
            </li>
        </ul>
    </nav>);

}

