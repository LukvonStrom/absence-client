import { h, Component } from 'preact';
import Authentication from '../utils/Auth.class';

export default class AsideComponent extends Component {

  constructor(props) {

        super(props);
        this.logout = this.logout.bind(this);
        const info = Authentication.getUserInfo();
        this.state = {
            name: info.name,
            grade: info.grade,
        }

    }

    logout() {

        Authentication.deauthenticate();

    }


    render({ classID}, {name, grade}) {

        return (
            <aside>
                <small>Angemeldet als</small>
                <big>
                    <span class="name">{name}</span>,&nbsp;
                    <span class="class">{grade}</span>
                </big>
                <button onClick={this.logout}>Abmelden</button>
            </aside>
        );

    }

}
