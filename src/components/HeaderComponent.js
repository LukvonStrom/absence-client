import { h, Component } from 'preact';
const schoolProperties = require('../schoolProperties');
export default class HeaderComponent extends Component{
    render(){
        return(
        <header>
            <div class="flex vertical-center">
                <img src="http://localhost:8080/logo" class="logo" alt="Schullogo" />
                <div class="name">
                    <h3>{schoolProperties.schooltitle}</h3>
                    <h2>{schoolProperties.short}</h2>
                </div>
            </div>
        </header>);
    }
}

