import Auth from "../../utils/Auth.class";
import {h, Component} from "preact";
import {isSupported, register} from "u2f-api";

export default class U2F extends Component {

    registerU2F = async (e) => {
        e.preventDefault();

        const support = await isSupported();
        if (!support) return this.props.triggerError("U2F aktiviert, aber Gerät inkompatibel!");

        const RegisterRequestRaw = await fetch('http://localhost:8080/u2f/register', {
            method: "GET",
            headers: {
                Authorization: Auth.getToken(),
            },
        });
        if (RegisterRequestRaw.json().err) this.props.triggerError(RegisterRequestRaw.json().err.message);
        const RegisterResponse = await register({registerRequests: RegisterRequestRaw.json()['Operation']});

        const rawSuccess = await fetch('http://localhost:8080/u2f/register', {
            method: "POST",
            headers: {
                Authorization: Auth.getToken(),
            },
            body: JSON.stringify({RegisterResponse})
        });

        const success = rawSuccess.json();

        if (!success.Operation.success) {
            return this.props.triggerError(success.Operation.message);
        }
    };

    render() {
        return (<button onClick={this.registerU2F}>U2F Ger&auml;t registrieren</button>)
    }
}