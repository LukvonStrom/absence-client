import { h, Component } from 'preact';
import U2F from "./U2FComponent";

export default class SettingsComponent extends Component {
    constructor(props){
        super(props);
        this.state = {
            settingsError: null,
        }
    }
    triggerError = (err) => {
            if(err.hasOwnProperty("message")) err = err.message;
            this.setState({settingsError: err});
    };
    render(props, {settingsError}){
        return (
            <main>
                <div class="flex headline">
                    <h1>Einstellungen</h1>
                </div>
                <p>
                    <h2>Sicherheit</h2>
                    <hr />
                    <br />
                    {settingsError && <div id="login_error" style={{'color': 'red'}}><p>{settingsError}</p></div>}
                    <h3>Passwort &auml;ndern</h3>
                    <hr />
                    <br />
                    <div id="login">
                        <form onSubmit={this.handleSubmit}>
                            <input id="new_password" type="password" placeholder="Neues Passwort" />
                            <input id="password_confirmation" type="password" placeholder="Passwort best&auml;tigen" />
                            <button type="submit">Passwort &auml;ndern</button>
                        </form>
                    </div>
                    <br />
                    <h3>Mehrstufige Anmeldung</h3>
                    <hr />

                    <div class="flex headline">
                        <U2F triggerError={this.triggerError}/>
                        <button>Authentikator basierte Anmeldung aktivieren</button>
                    </div>
                </p>
            </main>);
    }


}

