import { h, Component } from 'preact';
import './App.css';
import * as Raven from "raven-js";
// import ErrorBoundary from "./ErrorBoundary";
import HeaderComponent from "./components/HeaderComponent";
import LoginFormComponent from "./components/login/form/LoginFormComponent";
import AbsenceComponent from "./components/absence/AbsenceComponent";
import NavComponent from "./components/NavComponent";
import FooterComponent from "./components/FooterComponent";
import AsideComponent from "./components/AsideComponent";
import LoginContextComponent from "./components/login/LoginContextComponent";
import util from "./utils/util";
import Router, {route} from "preact-router";
import Match from "preact-router/match"
import apiFetch from "./utils/apiFetch";
import SettingsComponent from "./components/settings/SettingsComponent";

class App extends Component {
    /** @jsx h */
    _raven;
    constructor(props) {
        super(props);
        this._raven = Raven.config('https://97d62c4e987249e38d22a12572d42755@sentry.io/1104397').install();
        this.state =  {
            loginError: null,
        }
    }

    updateLogin(){
        this.setState()
    }

    isAuthenticated() {
        return new Promise((resolve, reject) => {
            apiFetch('authorized', 'GET').then(response => {
                if (response.auth) resolve(true);
                resolve(false);
            }).catch(e => resolve(false))
        })

    }

    authPaths=['/absence', '/settings'];

    handleRoute = e => {
    if((util.getAllURLParams().logout)){
        this.setState({loginError: '+Ausgeloggt'});
    }
    if(this.authPaths.includes(e.previous) && e.url === "/" && !(util.getAllURLParams().logout)){
        this.setState({loginError: 'Nicht authorisiert, bitte erneut anmelden'})
    }
    if(this.authPaths.includes(e.url)){
        this.isAuthenticated().then(isAuthed => {
            if(!isAuthed) return route('/', true);
        });
    }
    };

    // <ErrorBoundary raven={this._raven}></ErrorBoundary>
  render(props, {classID, name}) {
      const baseurl = "";

      const match = (url) => this.authPaths.includes(url)
    return (
        <div id="root" class="site">
            <HeaderComponent/>

            <Match path={baseurl + "/"}>
                { ({ matches }) => (
                    matches && <LoginFormComponent loginErrorMSG={this.state.loginError}/>
                ) }
            </Match>

            <Match path={baseurl + "/absence"}>
                { ({ url }) => match(url) && <NavComponent/> }
            </Match>
            <Match path={baseurl + "/absence"}>
                { ({ url }) => match(url) && <AsideComponent/> }
            </Match>

            <Router onChange={this.handleRoute.bind(this)}>
                <LoginContextComponent path={baseurl + "/"} default />
                <AbsenceComponent path={baseurl + "/absence"}/>
                <SettingsComponent path={baseurl + "/settings"} />
            </Router>
            <FooterComponent/>
        </div>
    );
  }
}

export default App;
