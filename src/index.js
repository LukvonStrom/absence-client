import { h, render } from 'preact';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

    let root;
    render(<App />, document.body, root);
    require('preact/devtools');
    registerServiceWorker();


